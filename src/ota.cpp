/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "ota.hpp"
#include <etl/algorithm.h>
#include <etl/utility.h>
#include <zephyr/random/random.h>
#include <zephyr/sys/reboot.h>

ota::ota()
    : m_session_secs(CONFIG_OTA_SESSION_TIMEOUT_SECS),
      m_ctx(0),
      m_active(false),
      m_seq_num(0),
      m_image_size(0),
      m_expire_ts(0) // Start as expired
{
}

uint32_t
ota::context() const
{
  return m_ctx;
}

bool
ota::active() const
{
  return m_active;
}

uint32_t
ota::seq_num() const
{
  return m_seq_num;
}

void
ota::begin(const struct _ota_request &req, ota_tlm &resp)
{
  if (m_active) {
    resp.ack_number = m_seq_num;
    resp.ota_ctx    = m_ctx;
    resp.code       = ota_tlm_response_code_OTA_SESSION_ACTIVE;
    m_expire_ts     = k_uptime_get() + m_session_secs * 1000;
    return;
  }

  if (req.img_len > max_image_size) {
    resp.ack_number = 0;
    resp.ota_ctx    = 0;
    resp.code       = ota_tlm_response_code_IMAGE_TOO_BIG;
    return;
  }

  //   // Erase the whole area
  //   int err = flash_img_init(&m_flash_img_ctx);
  //   if (err) {
  //     resp.ack_number = 0;
  //     resp.ota_ctx    = 0;
  //     resp.code       = ota_tlm_response_code_FLASH_FAILURE;
  //     resp.error_code = err;
  //     return;
  //   }

  //   err = flash_area_erase(m_flash_img_ctx.flash_area, 0,
  //                          m_flash_img_ctx.flash_area->fa_size);
  //   if (err) {
  //     resp.ack_number = 0;
  //     resp.ota_ctx    = 0;
  //     resp.code       = ota_tlm_response_code_FLASH_FAILURE;
  //     resp.error_code = err;
  //     return;
  //   }

  /* Everything ok, proceed normally */
  m_ctx        = sys_rand32_get();
  m_active     = true;
  m_seq_num    = 0;
  m_image_size = req.img_len;
  etl::copy_n(req.sha256.bytes, 32, m_hash);
  resp.ack_number = m_seq_num;
  resp.ota_ctx    = m_ctx;
  resp.code       = ota_tlm_response_code_OK;
  m_expire_ts     = k_uptime_get() + m_session_secs * 1000;
}

void
ota::finish(const struct _ota_finish &f, ota_tlm &resp)
{
  if (f.ctx != m_ctx || !m_active) {
    resp.ack_number = m_seq_num;
    resp.ota_ctx    = m_ctx;
    resp.code       = ota_tlm_response_code_INVALID_CTX;
    return;
  }

  resp.ack_number = m_seq_num;
  resp.ota_ctx    = m_ctx;

  //   /* use a dummy buffer of length 0 just to flush the outstanding bytes */
  //   int err = flash_img_buffered_write(&m_flash_img_ctx, m_hash, 0, true);
  //   if (err) {
  //     resp.code       = ota_tlm_response_code_INVALID_HASH;
  //     resp.error_code = err;
  //   }

  //   struct flash_img_check fic;
  //   fic.clen  = m_image_size;
  //   fic.match = m_hash;
  //   err       = flash_img_check(&m_flash_img_ctx, &fic,
  //                               FIXED_PARTITION_ID(slot1_partition));
  //   if (err) {
  //     resp.code       = ota_tlm_response_code_INVALID_HASH;
  //     resp.error_code = err;
  //   } else {
  //     resp.code = ota_tlm_response_code_OK;
  //     err       = boot_request_upgrade(BOOT_UPGRADE_TEST);
  //   }
  //   reset();
}

void
ota::packet(const struct _ota_data &d, ota_tlm &resp)
{
  if (d.ctx != m_ctx || !m_active) {
    resp.ack_number = m_seq_num;
    resp.ota_ctx    = m_ctx;
    resp.code       = ota_tlm_response_code_INVALID_CTX;
    return;
  }

  if (d.seq_number != m_seq_num) {
    resp.ack_number = m_seq_num;
    resp.ota_ctx    = m_ctx;
    resp.code       = ota_tlm_response_code_INVALID_SEQ_NUMBER;
    m_expire_ts     = k_uptime_get() + m_session_secs * 1000;
    return;
  }

  if (d.packet.size > msg_arbiter::mtu) {
    resp.ack_number = m_seq_num;
    resp.ota_ctx    = m_ctx;
    resp.code       = ota_tlm_response_code_INVALID_DATA;
    return;
  }

  if (d.packet.size + m_seq_num > m_image_size) {
    resp.ack_number = m_seq_num;
    resp.ota_ctx    = m_ctx;
    resp.code       = ota_tlm_response_code_IMAGE_TOO_BIG;
    return;
  }

  //   int err = flash_img_buffered_write(&m_flash_img_ctx, d.packet.bytes,
  //                                      d.packet.size, false);
  //   if (err) {
  //     resp.ack_number = m_seq_num;
  //     resp.ota_ctx    = m_ctx;
  //     resp.code       = ota_tlm_response_code_FLASH_FAILURE;
  //     resp.error_code = err;
  //     return;
  //   }

  m_seq_num += d.packet.size;
  resp.ack_number = m_seq_num;
  resp.ota_ctx    = m_ctx;
  resp.code       = ota_tlm_response_code_OK;

  m_expire_ts = k_uptime_get() + m_session_secs * 1000;
}

void
ota::reset()
{
  m_active  = false;
  m_seq_num = 0;
}

uint32_t
ota::session_duration() const
{
  return m_session_secs;
}

void
ota::set_session_duration(uint32_t secs)
{
  m_session_secs = secs;
}

void
ota::update()
{
  auto now = k_uptime_get();
  if (now > m_expire_ts) {
    reset();
  }
}
