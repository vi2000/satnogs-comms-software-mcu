/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include "tests/test.hpp"
#include <cstddef>
#include <cstdint>
#include <satnogs-comms-proto/satnogs-comms.pb.h>
#include <zephyr/kernel.h>

class msg_arbiter
{
public:
  static constexpr size_t mtu                = CONFIG_MAX_MTU;
  static constexpr size_t rx_msgq_size       = CONFIG_RX_MSGQ_SIZE;
  static constexpr size_t can1_tx_msgq_size  = CONFIG_CAN1_TX_MSGQ_SIZE;
  static constexpr size_t radio_tx_msgq_size = CONFIG_RADIO_TX_MSGQ_SIZE;

  static msg_arbiter &
  get_instance()
  {
    static msg_arbiter instance;
    return instance;
  }

  enum class subsys : uint8_t
  {
    CAN1        = 0,
    CAN2        = 1,
    RADIO_UHF   = 2,
    RADIO_SBAND = 3,
    SPI         = 4
  };

  /**
   * Message format that are used for internal routing
   *
   */
  class msg
  {
  public:
    subsys  iface;
    uint8_t data[mtu];
    size_t  len;
  };

  /* Singleton */
  msg_arbiter(msg_arbiter const &) = delete;

  void
  operator=(msg_arbiter const &) = delete;

  void
  push(const msg *m, bool block = false);

  int
  pull(msg *m, subsys s, k_timeout_t wait);

  int
  fwd(msg *m, subsys s, k_timeout_t wait);

  void
  start();

private:
  static void
  parse_thread(void *arg1, void *arg2, void *arg3);

  msg_arbiter();

  void
  parse_tlc(const satnogs_comms &msg, subsys iface);

  bool
  parse_tlc_test(const satnogs_comms &msg);

  bool
  parse_tlm_req(const satnogs_comms &msg);

  bool
  parse_periodic_tlm_req(const satnogs_comms &msg);

  bool
  parse_ota_tlc(const satnogs_comms &msg);

  struct k_msgq m_rx_msgq;
  struct k_msgq m_can1_tx_msgq;
  struct k_msgq m_radio_tx_msgq;

  char __aligned(4) m_rx_msgq_buffer[rx_msgq_size * sizeof(msg)];
  char __aligned(4) m_can1_tx_msgq_buffer[can1_tx_msgq_size * sizeof(msg)];
  char __aligned(4) m_radio_tx_msgq_buffer[radio_tx_msgq_size * sizeof(msg)];

  msg_arbiter::msg                   m_msg;
  struct k_thread                    m_thread_data;
  k_tid_t                            m_tid;
  struct k_mutex                     m_mtx;
  struct test::params_work_container m_test_params;
};
