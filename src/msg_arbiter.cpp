/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "msg_arbiter.hpp"
#include "callbacks.hpp"
#include "ota.hpp"
#include "telemetry.hpp"
#include "tests/test.hpp"
#include <pb_decode.h>
#include <pb_encode.h>
#include <satnogs-comms/version.hpp>
#include <zephyr/task_wdt/task_wdt.h>

K_THREAD_STACK_DEFINE(msg_arbiter_thread_stack, CONFIG_MSG_ARBITER_STACK_SIZE);
K_THREAD_STACK_DEFINE(async_workq_stack, CONFIG_ASYNC_WORKQUEUE_STACK_SIZE);

static struct k_work_q asynq_workq;

static satnogs_comms proto_recv;
static satnogs_comms proto_send;

void
msg_arbiter::push(const msg *m, bool block)
{
  if (block) {
    k_msgq_put(&m_rx_msgq, m, K_FOREVER);
  } else {
    while (k_msgq_put(&m_rx_msgq, m, K_NO_WAIT) != 0) {
      k_msgq_purge(&m_rx_msgq);
    }
  }
}

int
msg_arbiter::pull(msg *m, subsys s, k_timeout_t wait)
{
  switch (s) {
  case subsys::CAN1:
    return k_msgq_get(&m_can1_tx_msgq, m, wait);
  case subsys::RADIO_UHF:
    return k_msgq_get(&m_radio_tx_msgq, m, wait);
  default:
    return -EINVAL;
  }
}

int
msg_arbiter::fwd(msg *m, subsys s, k_timeout_t wait)
{
  switch (s) {
  case subsys::CAN1:
    return k_msgq_put(&m_can1_tx_msgq, m, wait);
  default:
    return -EINVAL;
  }
}
void
msg_arbiter::start()
{
  m_tid = k_thread_create(&m_thread_data, msg_arbiter_thread_stack,
                          K_THREAD_STACK_SIZEOF(msg_arbiter_thread_stack),
                          parse_thread, NULL, NULL, NULL,
                          CONFIG_MSG_ARBITER_PRIO, 0, K_NO_WAIT);
  if (!m_tid) {
    k_oops();
  }
  k_thread_name_set(m_tid, "msg_arbiter");

  const k_work_queue_config cfg = {.name = "asynq_workq", .no_yield = 0};
  k_work_queue_start(&asynq_workq, async_workq_stack,
                     K_THREAD_STACK_SIZEOF(async_workq_stack),
                     CONFIG_ASYNC_WORKQUEUE_PRIO, &cfg);

  k_work_init(&m_test_params.work, &test::exec);
}

void
msg_arbiter::parse_thread(void *arg1, void *arg2, void *arg3)
{
  int          task_wdt_id = task_wdt_add(CONFIG_WATCHDOG_PERIOD_MSG_ARBITER,
                                          task_wdt_callback, (void *)k_current_get());
  msg_arbiter &arb         = msg_arbiter::get_instance();
  while (1) {
    task_wdt_feed(task_wdt_id);
    if (k_msgq_get(&arb.m_rx_msgq, &arb.m_msg, K_MSEC(20)) == 0) {

      pb_istream_t stream =
          pb_istream_from_buffer(arb.m_msg.data, arb.m_msg.len);
      bool ok = pb_decode(&stream, satnogs_comms_fields, &proto_recv);
      if (!ok) {
        continue;
      }

      switch (proto_recv.which_mesages) {
      case satnogs_comms_tlc_tag:
        arb.parse_tlc(proto_recv, arb.m_msg.iface);
        break;
      default:
        break;
      }
    }
  }
}

msg_arbiter::msg_arbiter()
{
  k_msgq_init(&m_rx_msgq, m_rx_msgq_buffer, sizeof(struct msg), rx_msgq_size);
  k_msgq_init(&m_can1_tx_msgq, m_can1_tx_msgq_buffer, sizeof(struct msg),
              can1_tx_msgq_size);
  k_msgq_init(&m_radio_tx_msgq, m_radio_tx_msgq_buffer, sizeof(struct msg),
              radio_tx_msgq_size);
  k_mutex_init(&m_mtx);
}

void
msg_arbiter::parse_tlc(const satnogs_comms &msg, subsys iface)
{
  bool has_resp = false;
  switch (msg.mesages.tlc.which_telecommand) {
  case telecommands_ping_tag: {
    has_resp = true;
    telemetry::ping(proto_send, msg);
  } break;
  case telecommands_pwr_tag:
    has_resp = true;
    telemetry::power(proto_send);
    break;
  case telecommands_test_tag:
    has_resp = parse_tlc_test(msg);
    break;
  case telecommands_tlm_req_tag:
    has_resp = parse_tlm_req(msg);
    break;
  case telecommands_tlm_periodic_tag:
    has_resp = parse_periodic_tlm_req(msg);
    break;
  case telecommands_ota_tag:
    has_resp = parse_ota_tlc(msg);
    break;
  default:
    return;
  }

  if (has_resp == false) {
    return;
  }

  auto ostream = pb_ostream_from_buffer(m_msg.data, msg_arbiter::mtu);
  if (pb_encode(&ostream, satnogs_comms_fields, &proto_send) == false) {
    return;
  }
  m_msg.len = ostream.bytes_written;

  // FIXME interface handling needs revising
  switch (iface) {
  case subsys::CAN1:
    k_msgq_put(&m_can1_tx_msgq, &m_msg, K_FOREVER);
    break;
  case subsys::RADIO_UHF:
    k_msgq_put(&m_radio_tx_msgq, &m_msg, K_FOREVER);
  default:
    return;
  }
}

bool
msg_arbiter::parse_tlc_test(const satnogs_comms &msg)
{
  const auto &test_req                   = msg.mesages.tlc.telecommand.test;
  proto_send.which_mesages               = satnogs_comms_tlm_tag;
  proto_send.mesages.tlm.which_telemetry = telemetry_resp_ack_resp_tag;
  auto &ack = proto_send.mesages.tlm.telemetry.ack_resp;
  auto &t   = test::get_instance();

  switch (test_req.which_tests) {
  case test_req_start_tag: {
    if (t.test_valid(test_req.tests.start.id) == false) {
      ack.success = false;
      ack.err     = error_UNKNOWN_TEST;
      return true;
    }
    m_test_params.test_id = test_req.tests.start.id;
    m_test_params.param0  = test_req.tests.start.param0;
    m_test_params.param1  = test_req.tests.start.param1;

    int ret = k_work_submit_to_queue(&asynq_workq, &m_test_params.work);
    if (ret >= 0) {
      ack.success    = true;
      ack.err        = error_NO_ERROR;
      ack.error_code = ret;
    } else {
      ack.success    = false;
      ack.err        = error_EXEC_FAIL;
      ack.error_code = ret;
    }
  } break;
  /* Stop any running test */
  case test_req_stop_tag:
    t.stop();
    ack.success = true;
    ack.err     = error_NO_ERROR;
    break;
  default:
    ack.success = false;
    ack.err     = error_INVALID_CMD;
  }
  return true;
}

bool
msg_arbiter::parse_tlm_req(const satnogs_comms &msg)
{
  const auto &tlm_req      = msg.mesages.tlc.telecommand.tlm_req;
  proto_send.which_mesages = satnogs_comms_tlm_tag;
  telemetry::tlm(proto_send, tlm_req.type);
  return true;
}

bool
msg_arbiter::parse_periodic_tlm_req(const satnogs_comms &msg)
{
  auto &req = msg.mesages.tlc.telecommand.tlm_periodic;
  auto &tlm = telemetry::get_instance();

  if (req.enable) {
    tlm.enable(req.type, req.period_ms);
  } else {
    tlm.stop();
  }

  proto_send.which_mesages               = satnogs_comms_tlm_tag;
  proto_send.mesages.tlm.which_telemetry = telemetry_resp_ack_resp_tag;
  auto &ack      = proto_send.mesages.tlm.telemetry.ack_resp;
  ack.success    = true;
  ack.error_code = error_NO_ERROR;
  ack.error_code = 0;
  return true;
}

bool
msg_arbiter::parse_ota_tlc(const satnogs_comms &msg)
{
  const auto &ota_msg = msg.mesages.tlc.telecommand.ota;
  auto       &ota     = ota::get_instance();

  proto_send.which_mesages               = satnogs_comms_tlm_tag;
  proto_send.mesages.tlm.which_telemetry = telemetry_resp_ota_resp_tag;

  switch (ota_msg.which_messages) {
  case ota_tlc_request_tag:
    ota.begin(ota_msg.messages.request,
              proto_send.mesages.tlm.telemetry.ota_resp);
    break;
  case ota_tlc_data_tag:
    ota.packet(ota_msg.messages.data,
               proto_send.mesages.tlm.telemetry.ota_resp);
    break;
  case ota_tlc_finish_tag:
    ota.finish(ota_msg.messages.finish,
               proto_send.mesages.tlm.telemetry.ota_resp);
    break;
  default:
    return false;
  }
  return true;
}
