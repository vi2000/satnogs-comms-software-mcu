/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */
#include "telemetry.hpp"
#include "callbacks.hpp"
#include "msg_arbiter.hpp"
#include <pb_encode.h>
#include <satnogs-comms/power.hpp>
#include <satnogs-comms/radio.hpp>
#include <satnogs-comms/version.hpp>
#include <version.hpp>
#include <zephyr/task_wdt/task_wdt.h>

K_THREAD_STACK_DEFINE(telemetry_thread_stack,
                      CONFIG_TELEMETRY_THREAD_STACK_SIZE);

namespace sc = satnogs::comms;

static msg_arbiter::msg tlm_msg;
static satnogs_comms    proto_msg;

void
telemetry::to_pwr_msg(::power &msg)
{
  const auto &pwr      = sc::board::get_instance().power();
  msg.can1_en          = pwr.enabled(sc::power::subsys::CAN1);
  msg.can1_low_pwr     = pwr.enabled(sc::power::subsys::CAN1_LPWR);
  msg.can2_en          = pwr.enabled(sc::power::subsys::CAN2);
  msg.can2_low_pwr     = pwr.enabled(sc::power::subsys::CAN2_LPWR);
  msg.fpga_en          = pwr.enabled(sc::power::subsys::FPGA_5V);
  msg.rf_5v_en         = pwr.enabled(sc::power::subsys::RF_5V);
  msg.fpga_voltage     = pwr.voltage(sc::power::channel::FPGA);
  msg.fpga_current     = pwr.current(sc::power::channel::FPGA);
  msg.rf_uhf_voltage   = pwr.voltage(sc::power::channel::RF_5V_U);
  msg.rf_uhf_current   = pwr.current(sc::power::channel::RF_5V_U);
  msg.rf_sband_voltage = pwr.voltage(sc::power::channel::RF_5V_S);
  msg.rf_sband_current = pwr.current(sc::power::channel::RF_5V_S);
  msg.rf_5v_pgood      = pwr.pgood(sc::power::pgood_tp::RAIL_5V);
  msg.rf_3v3_pgood     = pwr.pgood(sc::power::pgood_tp::RAIL_3V3);
  msg.fpga_pgood       = pwr.pgood(sc::power::pgood_tp::RAIL_FPGA);
  msg.vin              = pwr.voltage(sc::power::channel::VIN);
  msg.iin              = pwr.current(sc::power::channel::VIN);
  msg.uhf_en           = pwr.enabled(sc::power::subsys::UHF);
  msg.uhf_pgood        = pwr.pgood(sc::power::pgood_tp::RAIL_UHF);
  msg.sband_en         = pwr.enabled(sc::power::subsys::SBAND);
  msg.sband_pgood      = pwr.pgood(sc::power::pgood_tp::RAIL_SBAND);
}

void
telemetry::to_sensors_msg(sensors &msg)
{
  auto &board          = sc::board::get_instance();
  msg.has_temp         = true;
  msg.temp.pcb         = board.temperature(sc::temperature_sensor::PCB);
  msg.temp.uhf_pa      = board.temperature(sc::temperature_sensor::UHF_PA);
  msg.temp.sband_pa    = board.temperature(sc::temperature_sensor::SBAND_PA);
  msg.temp.uhf_alert   = board.alert(sc::temperature_sensor::UHF_PA);
  msg.temp.sband_alert = board.alert(sc::temperature_sensor::SBAND_PA);
}

static void
fill_radio_stats(const sc::radio &radio, sc::radio::interface iface,
                 radio_stats &s)
{
  const auto &stats = radio.get_stats(iface);
  s.tx_frames       = stats.tx_frames;
  s.tx_frames_fail  = stats.tx_frames_fail;
  s.tx_frames_drop  = stats.tx_frames_drop;
  s.rx_frames       = stats.rx_frames;
  s.rx_frames_inval = stats.rx_frames_inval;
  s.rx_frames_drop  = stats.rx_frames_drop;
}

void
telemetry::to_radio_uhf(radio_uhf &msg)
{
  auto       &board = sc::board::get_instance();
  auto       &radio = board.radio();
  const auto &pwr   = board.power();
  const auto &rf09  = radio.uhf();

  msg.enabled = radio.enabled(sc::radio::interface::UHF);
  auto dir    = radio.direction(sc::radio::interface::UHF);
  if (dir == sc::rf_frontend::dir::RX) {
    msg.dir = radio_dir::radio_dir_RX;
  } else {
    msg.dir = radio_dir::radio_dir_TX;
  }
  msg.fault = !pwr.pgood(sc::power::pgood_tp::RAIL_UHF);
  if (rf09.get_filter() == sc::rf_frontend::filter::NARROW) {
    msg.filter = filter_type::filter_type_NARROW;
  } else {
    msg.filter = filter_type::filter_type_WIDE;
  }

  if (rf09.gain_mode() == sc::rf_frontend::gain_mode::AUTO) {
    msg.agc = agc_mode::agc_mode_AUTO;
  } else {
    msg.agc = agc_mode::agc_mode_MANUAL;
  }
  msg.gain = rf09.gain();

  msg.has_stats = true;
  fill_radio_stats(radio, sc::radio::interface::UHF, msg.stats);
}

void
telemetry::to_radio_sband(radio_sband &msg)
{
  auto       &board = sc::board::get_instance();
  auto       &radio = board.radio();
  const auto &pwr   = board.power();
  auto       &rf24  = radio.sband();

  msg.enabled = radio.enabled(sc::radio::interface::SBAND);
  auto dir    = radio.direction(sc::radio::interface::SBAND);
  if (dir == sc::rf_frontend::dir::RX) {
    msg.dir = radio_dir::radio_dir_RX;
  } else {
    msg.dir = radio_dir::radio_dir_TX;
  }
  msg.mixer_lock = radio.mixer_lock();
  msg.fault      = !pwr.pgood(sc::power::pgood_tp::RAIL_SBAND);
  if (rf24.get_filter() == sc::rf_frontend::filter::NARROW) {
    msg.filter = filter_type::filter_type_NARROW;
  } else {
    msg.filter = filter_type::filter_type_WIDE;
  }

  if (rf24.gain_mode() == sc::rf_frontend::gain_mode::AUTO) {
    msg.agc = agc_mode::agc_mode_AUTO;
  } else {
    msg.agc = agc_mode::agc_mode_MANUAL;
  }
  msg.gain = rf24.gain();

  msg.has_stats = true;
  fill_radio_stats(radio, sc::radio::interface::SBAND, msg.stats);
}

void
telemetry::to_version(version &msg_hw, version &msg_lib, version &msg_fw)
{
  msg_hw.major  = static_cast<uint32_t>(sc::version::hw_major);
  msg_hw.minor  = static_cast<uint32_t>(sc::version::hw_minor);
  msg_hw.patch  = static_cast<uint32_t>(sc::version::hw_patch);
  msg_lib.major = static_cast<uint32_t>(sc::version::lib_major);
  msg_lib.minor = static_cast<uint32_t>(sc::version::lib_minor);
  msg_lib.patch = static_cast<uint32_t>(sc::version::lib_patch);
  msg_fw.major  = static_cast<uint32_t>(satnogs::version::fw_major);
  msg_fw.minor  = static_cast<uint32_t>(satnogs::version::fw_minor);
  msg_fw.patch  = static_cast<uint32_t>(satnogs::version::fw_patch);
}

void
telemetry::power(satnogs_comms &msg)
{
  msg.which_mesages               = satnogs_comms_tlm_tag;
  msg.mesages.tlm.which_telemetry = telemetry_resp_pwr_tag;
  auto &pwr_msg                   = msg.mesages.tlm.telemetry.pwr;
  to_pwr_msg(pwr_msg);
}

void
telemetry::tlm(satnogs_comms &msg, telemetry_type type)
{
  msg.which_mesages = satnogs_comms_tlm_tag;
  switch (type) {
  case telemetry_type::telemetry_type_BASIC: {
    msg.mesages.tlm.which_telemetry = telemetry_resp_tlm_basic_tag;
    auto &tlm_msg                   = msg.mesages.tlm.telemetry.tlm_basic;
    tlm_msg.uptime_ms               = k_uptime_get();
    tlm_msg.has_pwr                 = true;
    tlm_msg.has_uhf                 = true;
    tlm_msg.has_sband               = true;
    to_pwr_msg(tlm_msg.pwr);
    to_radio_uhf(tlm_msg.uhf);
    to_radio_sband(tlm_msg.sband);
  } break;
  case telemetry_type::telemetry_type_HEALTH: {
    msg.mesages.tlm.which_telemetry = telemetry_resp_tlm_health_tag;
    auto &tlm_health_msg            = msg.mesages.tlm.telemetry.tlm_health;
    tlm_health_msg.uptime_ms        = k_uptime_get();
    tlm_health_msg.has_sens         = true;
    tlm_health_msg.has_pwr          = true;
    to_sensors_msg(tlm_health_msg.sens);
    to_pwr_msg(tlm_health_msg.pwr);
  } break;
  case telemetry_type::telemetry_type_CONFIG: {
    msg.mesages.tlm.which_telemetry = telemetry_resp_tlm_cnf_tag;
    auto &tlm_cnf_msg               = msg.mesages.tlm.telemetry.tlm_cnf;
    tlm_cnf_msg.has_ver_hw          = true;
    tlm_cnf_msg.has_ver_lib         = true;
    tlm_cnf_msg.has_ver_fw          = true;
    to_version(tlm_cnf_msg.ver_hw, tlm_cnf_msg.ver_lib, tlm_cnf_msg.ver_fw);
  } break;
  case telemetry_type::telemetry_type_FPGA: {
    msg.mesages.tlm.which_telemetry = telemetry_resp_fpga_tag;
    auto &fpga_msg                  = msg.mesages.tlm.telemetry.fpga;
    auto &fpga                      = sc::board::get_instance().fpga();
    fpga_msg.enabled                = fpga.enabled();
    // TO DO: Add correct methods when implemented in libsatnogs-comms
    fpga_msg.done   = false;
    fpga_msg.emmc   = false;
    fpga_msg.rx_irq = false;
    fpga_msg.ip_rst = false;
  } break;
  case telemetry_type::telemetry_type_RADIO: {
    msg.mesages.tlm.which_telemetry = telemetry_resp_radio_tag;
    auto &radio_msg                 = msg.mesages.tlm.telemetry.radio;
    radio_msg.has_uhf               = true;
    radio_msg.has_sband             = true;
    to_radio_uhf(radio_msg.uhf);
    to_radio_sband(radio_msg.sband);
    radio_msg.enabled = radio_msg.sband.enabled || radio_msg.uhf.enabled;
  } break;
  default: {
    msg.mesages.tlm.which_telemetry = telemetry_resp_ack_resp_tag;
    auto &ack                       = msg.mesages.tlm.telemetry.ack_resp;
    ack.success                     = false;
    ack.err                         = error_INVALID_CMD;
  } break;
  }
}

void
telemetry::ping(satnogs_comms &resp, const satnogs_comms &recv)
{
  const auto &ping                 = recv.mesages.tlc.telecommand.ping;
  resp.which_mesages               = satnogs_comms_tlm_tag;
  resp.mesages.tlm.which_telemetry = telemetry_resp_ping_tag;
  size_t size = std::min<size_t>(ping.payload.size, msg_arbiter::mtu);
  resp.mesages.tlm.telemetry.ping.payload.size = size;
  memcpy(resp.mesages.tlm.telemetry.ping.payload.bytes, ping.payload.bytes,
         size);
}

void
telemetry::start()
{
  m_tid = k_thread_create(&m_thread_data, telemetry_thread_stack,
                          K_THREAD_STACK_SIZEOF(telemetry_thread_stack),
                          periodic_thread, NULL, NULL, NULL,
                          CONFIG_TELEMETRY_THREAD_PRIO, 0, K_NO_WAIT);
  if (!m_tid) {
    k_oops();
  }
  k_thread_name_set(m_tid, "telemetry");
}

telemetry::telemetry()
    : m_enable_periodic(false),
      m_period_ms(min_period_ms),
      m_type(telemetry_type::telemetry_type_BASIC)
{
}

void
telemetry::periodic_thread(void *arg1, void *arg2, void *arg3)
{
  auto &tlm         = telemetry::get_instance();
  int   task_wdt_id = task_wdt_add(CONFIG_WATCHDOG_PERIOD_TELEMETRY,
                                   task_wdt_callback, (void *)k_current_get());

  while (1) {
    // Avoid wdt timeout in case of large period
    for (uint32_t i = 0; i < tlm.m_period_ms / 2048; i++) {
      task_wdt_feed(task_wdt_id);
      k_msleep(2048);
    }
    k_msleep(tlm.m_period_ms % 2048);
    task_wdt_feed(task_wdt_id);

    if (tlm.m_enable_periodic) {
      auto &arb = msg_arbiter::get_instance();
      tlm.tlm(proto_msg, tlm.m_type);

      auto ostream = pb_ostream_from_buffer(tlm_msg.data, msg_arbiter::mtu);
      if (pb_encode(&ostream, satnogs_comms_fields, &proto_msg) == false) {
        continue;
      }
      tlm_msg.len   = ostream.bytes_written;
      tlm_msg.iface = msg_arbiter::subsys::CAN1;
      arb.fwd(&tlm_msg, msg_arbiter::subsys::CAN1, K_FOREVER);
    }
  }
}

void
telemetry::enable(telemetry_type type, uint32_t period_ms)
{
  m_type            = type;
  m_period_ms       = std::max(period_ms, min_period_ms);
  m_enable_periodic = true;
}

void
telemetry::stop()
{
  m_enable_periodic = false;
  m_period_ms       = min_period_ms;
}
