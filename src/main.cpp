/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2022-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <zephyr/kernel.h>

#include "bsp/bsp.hpp"
#include "etl_profile.h"
#include "version.hpp"
#include <errno.h>
#include <satnogs-comms/board.hpp>
#include <zephyr/devicetree.h>
#include <zephyr/logging/log.h>
/* Register the log module */
LOG_MODULE_REGISTER(satnogscomms);

#include <zephyr/stats/stats.h>
#include <zephyr/sys/reboot.h>
#include <zephyr/sys/util.h>
#include <zephyr/task_wdt/task_wdt.h>

#include "callbacks.hpp"
#include "io.hpp"
#include "msg_arbiter.hpp"
#include "telemetry.hpp"

namespace sc = satnogs::comms;

#if DT_HAS_COMPAT_STATUS_OKAY(st_stm32_watchdog)
#define WDT_NODE DT_COMPAT_GET_ANY_STATUS_OKAY(st_stm32_watchdog)
#else
#error "IWDG is not available. Check the device tree configuration"
#endif

const struct gpio_dt_spec led0 = GPIO_DT_SPEC_GET(DT_ALIAS(led0), gpios);
const struct gpio_dt_spec led1 = GPIO_DT_SPEC_GET(DT_ALIAS(led1), gpios);

const struct spi_config radio_spi_cfg = {
    .frequency = 5000000,
    .operation = SPI_OP_MODE_MASTER | SPI_TRANSFER_MSB | SPI_WORD_SET(8) |
                 SPI_LINES_SINGLE | SPI_FULL_DUPLEX | SPI_FRAME_FORMAT_MOTOROLA,
    .cs = {.gpio  = SPI_CS_GPIOS_DT_SPEC_GET(DT_NODELABEL(radio_spi)),
           .delay = 4}};
const struct device      *radio_spi_dev = DEVICE_DT_GET(DT_NODELABEL(spi4));
const struct gpio_dt_spec radio_rst =
    GPIO_DT_SPEC_GET(DT_NODELABEL(radio_rst), gpios);

const struct spi_config fpga_spi_cfg = {
    .frequency = 5000000,
    .operation = SPI_OP_MODE_MASTER | SPI_TRANSFER_MSB | SPI_WORD_SET(8) |
                 SPI_LINES_SINGLE | SPI_FULL_DUPLEX | SPI_FRAME_FORMAT_MOTOROLA,
    .cs = {.gpio  = SPI_CS_GPIOS_DT_SPEC_GET(DT_NODELABEL(fpga_spi)),
           .delay = 4}};
const struct device *fpga_spi_dev = DEVICE_DT_GET(DT_NODELABEL(spi2));

const struct gpio_dt_spec p5v_rf_en =
    GPIO_DT_SPEC_GET(DT_NODELABEL(p5v_rf_en), gpios);
const struct gpio_dt_spec p5v_fpga_en =
    GPIO_DT_SPEC_GET(DT_NODELABEL(p5v_fpga_en), gpios);
const struct device *sens_i2c = DEVICE_DT_GET(DT_ALIAS(sensorsi2c));

const struct gpio_dt_spec can1_en =
    GPIO_DT_SPEC_GET(DT_NODELABEL(can1_en), gpios);
const struct gpio_dt_spec can1_low_pwr =
    GPIO_DT_SPEC_GET(DT_NODELABEL(can1_low_pwr), gpios);
const struct gpio_dt_spec can2_en =
    GPIO_DT_SPEC_GET(DT_NODELABEL(can2_en), gpios);
const struct gpio_dt_spec can2_low_pwr =
    GPIO_DT_SPEC_GET(DT_NODELABEL(can2_low_pwr), gpios);

const struct gpio_dt_spec p5v_rf_pg =
    GPIO_DT_SPEC_GET(DT_NODELABEL(p5v_rf_pg), gpios);
const struct gpio_dt_spec p5v_fpga_pg =
    GPIO_DT_SPEC_GET(DT_NODELABEL(p5v_fpga_pg), gpios);

#if DT_NODE_EXISTS(DT_NODELABEL(p3v3_rf_pg))
const struct gpio_dt_spec p3v3_rf_pg =
    GPIO_DT_SPEC_GET(DT_NODELABEL(p3v3_rf_pg), gpios);
#else
const struct gpio_dt_spec p3v3_rf_pg = {.port = nullptr, 0, 0};
#endif

const struct gpio_dt_spec en_uhf =
    GPIO_DT_SPEC_GET(DT_NODELABEL(en_uhf), gpios);
const struct gpio_dt_spec uhf_pgood =
    GPIO_DT_SPEC_GET(DT_NODELABEL(uhf_pgood), gpios);
const struct gpio_dt_spec en_sband =
    GPIO_DT_SPEC_GET(DT_NODELABEL(en_sband), gpios);
const struct gpio_dt_spec sband_pgood =
    GPIO_DT_SPEC_GET(DT_NODELABEL(sband_pgood), gpios);

const struct gpio_dt_spec en_agc_uhf =
    GPIO_DT_SPEC_GET(DT_NODELABEL(en_agc_uhf), gpios);
const struct gpio_dt_spec flt_sel_uhf =
    GPIO_DT_SPEC_GET(DT_NODELABEL(flt_sel_uhf), gpios);
const struct gpio_dt_spec alert_uhf_pa_temp =
    GPIO_DT_SPEC_GET(DT_NODELABEL(alert_uhf_pa_temp), gpios);

const struct gpio_dt_spec mixer_rst =
    GPIO_DT_SPEC_GET(DT_NODELABEL(mixer_rst), gpios);
const struct gpio_dt_spec mixer_enx =
    GPIO_DT_SPEC_GET(DT_NODELABEL(mixer_enx), gpios);
const struct gpio_dt_spec mixer_scl =
    GPIO_DT_SPEC_GET(DT_NODELABEL(mixer_scl), gpios);
const struct gpio_dt_spec mixer_sda =
    GPIO_DT_SPEC_GET(DT_NODELABEL(mixer_sda), gpios);

const struct gpio_dt_spec en_agc_sband =
    GPIO_DT_SPEC_GET(DT_NODELABEL(en_agc_sband), gpios);
const struct gpio_dt_spec flt_sel_sband =
    GPIO_DT_SPEC_GET(DT_NODELABEL(flt_sel_sband), gpios);
const struct gpio_dt_spec alert_sband_pa_temp =
    GPIO_DT_SPEC_GET(DT_NODELABEL(alert_sband_pa_temp), gpios);

const struct gpio_dt_spec emmc_en =
    GPIO_DT_SPEC_GET(DT_NODELABEL(emmc_en), gpios);
const struct gpio_dt_spec emmc_sel =
    GPIO_DT_SPEC_GET(DT_NODELABEL(emmc_sel), gpios);

const struct device *adc1 = DEVICE_DT_GET(DT_NODELABEL(adc1));

#if DT_NODE_HAS_STATUS(DT_NODELABEL(adc3), okay)
const struct device *adc3 = DEVICE_DT_GET(DT_NODELABEL(adc3));
#else
const struct device *adc3 = nullptr;
#endif
const struct device *dac1 = DEVICE_DT_GET(DT_NODELABEL(dac1));

K_THREAD_STACK_DEFINE(workqueue_thread_stack, CONFIG_MAIN_WORKQUEUE_STACK_SIZE);
struct k_work_q main_work_q;

const struct gpio_dt_spec radio_trx_irq =
    GPIO_DT_SPEC_GET(DT_NODELABEL(radio_trx_irq), gpios);

static void
radio_trx_irq_handler(struct k_work *item)
{
  sc::radio::trx_irq_handler();
}
struct k_work        radio_trx_irq_work;
struct gpio_callback radio_trx_irq_clbk_h;

static void
radio_trx_irq_callback(const struct device *dev, struct gpio_callback *cb,
                       uint32_t pins)
{
  k_work_submit(&radio_trx_irq_work);
}

/* Misc IO */
gpio_bsp l0(&led0);
gpio_bsp l1(&led1);

/* FPGA IO*/
spi_bsp fpga_spi(fpga_spi_dev, fpga_spi_cfg);

/* Power IO */
i2c_bsp  sensors_i2c(sens_i2c);
gpio_bsp gpio_p5v_rf(&p5v_rf_en);
gpio_bsp gpio_p5v_fpga(&p5v_fpga_en);
gpio_bsp gpio_can1_en(&can1_en);
gpio_bsp gpio_can1_low_pwr(&can1_low_pwr);
gpio_bsp gpio_can2_en(&can2_en);
gpio_bsp gpio_can2_low_pwr(&can2_low_pwr);
gpio_bsp gpio_rf_5v_pgood(&p5v_rf_pg, false);
gpio_bsp gpio_fpga_5v_pgood(&p5v_fpga_pg, false);
gpio_bsp gpio_en_uhf(&en_uhf);
gpio_bsp gpio_uhf_pgood(&uhf_pgood, false);
gpio_bsp gpio_en_sband(&en_sband);
gpio_bsp gpio_sband_pgood(&sband_pgood, false);

/* Radio IO */
gpio_bsp gpio_en_agc_uhf(&en_agc_uhf);
gpio_bsp gpio_flt_sel_uhf(&flt_sel_uhf);
gpio_bsp gpio_alert_uhf_pa_temp(&alert_uhf_pa_temp, false);
gpio_bsp gpio_en_agc_sband(&en_agc_sband);
gpio_bsp gpio_flt_sel_sband(&flt_sel_sband);
gpio_bsp gpio_alert_sband_pa_temp(&alert_sband_pa_temp, false);

spi_bsp  radio_spi(radio_spi_dev, radio_spi_cfg);
gpio_bsp radio_nrst(&radio_rst);
gpio_bsp gpio_mixer_rst(&mixer_rst);
gpio_bsp gpio_mixer_enx(&mixer_enx);
gpio_bsp gpio_mixer_scl(&mixer_scl);
gpio_bsp gpio_mixer_sda(&mixer_sda);

dac_bsp agc_vout_uhf(dac1, sc::radio::UHF_AGC_VOUT_DAC_CH);
dac_bsp agc_vout_sband(dac1, sc::radio::SBAND_AGC_VOUT_DAC_CH);

/* EMMC IO */
gpio_bsp gpio_emmc_en(&emmc_en);
gpio_bsp gpio_emmc_sel(&emmc_sel);

chrono_bsp chr;

/* Handling different IO of the power subsystem */
auto pwr_cnf = []() -> auto {
  if constexpr (sc::version::hw() > sc::version::num(0, 2)) {
    static adc_bsp adc_imon_3v3_d(adc1, sc::power_v0_3::IMOV_3V3_D_ADC_CH);
    static adc_bsp adc_imon_5v_rf(adc3, sc::power_v0_3::IMOV_5V_RF_ADC_CH);
    static adc_bsp adc_imon_5v_fpga(adc3, sc::power_v0_3::IMOV_5V_FPGA_ADC_CH);
    sc::power_v0_3::i_lim   current_limit(CONFIG_IMON_3V3_D_LIM / 1000.0f,
                                          CONFIG_IMON_5V_FPGA_LIM / 1000.0f,
                                          CONFIG_IMON_5V_RF_LIM / 1000.0f);
    sc::power_v0_3::io_conf cnf = {.mon_i2c       = sensors_i2c,
                                   .rf_5v_en      = gpio_p5v_rf,
                                   .fpga_5v_en    = gpio_p5v_fpga,
                                   .can1_en       = gpio_can1_en,
                                   .can1_low_pwr  = gpio_can1_low_pwr,
                                   .can2_en       = gpio_can2_en,
                                   .can2_low_pwr  = gpio_can2_low_pwr,
                                   .rf_5v_pgood   = gpio_rf_5v_pgood,
                                   .fpga_5v_pgood = gpio_fpga_5v_pgood,
                                   .uhf_en        = gpio_en_uhf,
                                   .uhf_pgood     = gpio_uhf_pgood,
                                   .sband_en      = gpio_en_sband,
                                   .sband_pgood   = gpio_sband_pgood,
                                   .imon_3v3_d    = adc_imon_3v3_d,
                                   .imon_5v_rf    = adc_imon_5v_rf,
                                   .imon_fpga     = adc_imon_5v_fpga,
                                   .ilim          = current_limit};
    return cnf;
  } else {
    static gpio_bsp         gpio_rf_3v3_pgood(&p3v3_rf_pg, false);
    sc::power_v0_2::io_conf cnf = {.mon_i2c       = sensors_i2c,
                                   .rf_5v_en      = gpio_p5v_rf,
                                   .fpga_5v_en    = gpio_p5v_fpga,
                                   .can1_en       = gpio_can1_en,
                                   .can1_low_pwr  = gpio_can1_low_pwr,
                                   .can2_en       = gpio_can2_en,
                                   .can2_low_pwr  = gpio_can2_low_pwr,
                                   .rf_5v_pgood   = gpio_rf_5v_pgood,
                                   .rf_3v3_pgood  = gpio_rf_3v3_pgood,
                                   .fpga_5v_pgood = gpio_fpga_5v_pgood};
    return cnf;
  }
}();

msgq<sc::radio::rx_msg, CONFIG_RADIO_RX_MSGQ_SIZE> rx_msgq;

sc::rf_frontend09::io_conf rffe09_io = {.en_agc   = gpio_en_agc_uhf,
                                        .agc_vset = agc_vout_uhf,
                                        .flt_sel  = gpio_flt_sel_uhf};

sc::rf_frontend24::io_conf rffe24_io = {.en_agc    = gpio_en_agc_sband,
                                        .agc_vset  = agc_vout_sband,
                                        .flt_sel   = gpio_flt_sel_sband,
                                        .mixer_clk = gpio_mixer_scl,
                                        .mixer_rst = gpio_mixer_rst,
                                        .mixer_enx = gpio_mixer_enx,
                                        .mixer_sda = gpio_mixer_sda,
                                        .chrono    = chr};

sc::radio::io_conf radio_cnf = {.spi_ctrl  = radio_spi,
                                .nreset    = radio_nrst,
                                .chrono    = chr,
                                .rffe09_io = rffe09_io,
                                .rffe24_io = rffe24_io};

sc::board::io_conf board_io = {.emmc_en          = gpio_emmc_en,
                               .emmc_sel         = gpio_emmc_sel,
                               .fpga_spi         = radio_spi,
                               .led0             = l0,
                               .led1             = l1,
                               .pwr_io           = pwr_cnf,
                               .sensors_i2c      = sensors_i2c,
                               .alert_t_pa_uhf   = gpio_alert_uhf_pa_temp,
                               .alert_t_pa_sband = gpio_alert_sband_pa_temp,
                               .radio_io         = radio_cnf,
                               .chrono           = chr};

sc::board::params board_params = {.fpga_hw       = sc::fpga::hw::ALINX_AC7Z020,
                                  .emmc_capacity = 8ULL * 1024 * 1024 * 1024,
                                  .emmc_start    = 0,
                                  .emmc_stop     = 100ULL * 1024 * 1024,
                                  .rx_msgq       = rx_msgq};

int
main(void)
{
  /*
   * Before do anything enable the hardware watchdog!
   * Ideally, it should be already enabled by the bootloader!
   */
  const struct device *hw_wdt_dev = DEVICE_DT_GET_OR_NULL(WDT_NODE);

  if (!device_is_ready(hw_wdt_dev)) {
    printk("Hardware watchdog not ready; ignoring it.\n");
  }

  int ret = task_wdt_init(hw_wdt_dev);
  if (ret) {
    printk("task wdt init failure: %d\n", ret);
    k_oops();
  }

  int wdt_id = task_wdt_add(CONFIG_WATCHDOG_PERIOD_SYS, task_wdt_callback,
                            (void *)k_current_get());
  /*
   * task_wdt_add() does activates the IWDG internally. The IWDG is enabled at
   * the first call of task_wdt_feed(). So it is essential for the
   * task_wdt_feed() to be called before any code that may hang the MCU
   */
  task_wdt_feed(wdt_id);

  sc::board::init(board_io, board_params);

  auto &board = sc::board::get_instance();
  auto &radio = board.radio();
  auto &fpga  = board.fpga();
  auto &emmc  = board.emmc();
  auto &leds  = board.leds();

  auto &msg_arb = msg_arbiter::get_instance();
  auto &io_ctrl = io::get_instance();

  radio.enable();
  radio.enable(sc::radio::interface::UHF, false);
  radio.enable(sc::radio::interface::SBAND, false);
  fpga.enable();
  emmc.enable();
  task_wdt_feed(wdt_id);

  /*
   * Initialize the main work queue that can accept workers. This a
   * good practice instead of executing directly stuff into the timer.
   *
   * Timers on Zephyr operate in interrupt context.
   */
  k_work_queue_start(&main_work_q, workqueue_thread_stack,
                     K_THREAD_STACK_SIZEOF(workqueue_thread_stack),
                     CONFIG_MAIN_WORKQUEUE_PRIO, NULL);

  /* Spawn the threads */
  msg_arb.start();
  io_ctrl.start();
  telemetry::get_instance().start();
  task_wdt_feed(wdt_id);

  /* Now that the main work queue is up and the radio has been initialized
   * activate the IRQ */
  gpio_pin_configure_dt(&radio_trx_irq, GPIO_INPUT | radio_trx_irq.dt_flags);

  gpio_init_callback(&radio_trx_irq_clbk_h, radio_trx_irq_callback,
                     BIT(radio_trx_irq.pin));
  gpio_add_callback(radio_trx_irq.port, &radio_trx_irq_clbk_h);
  k_work_init(&radio_trx_irq_work, radio_trx_irq_handler);
  gpio_pin_interrupt_configure_dt(&radio_trx_irq, GPIO_INT_EDGE_TO_ACTIVE);

  radio.set_frequency(sc::radio::interface::UHF, sc::rf_frontend::dir::RX,
                      435e6);
  radio.uhf().set_gain_mode(sc::rf_frontend::gain_mode::AUTO);
  radio.uhf().set_filter(sc::rf_frontend::filter::WIDE);
  radio.rx_async(sc::radio::interface::UHF);
  while (1) {
    task_wdt_feed(wdt_id);
    k_msleep(1000);
    leds.toggle(sc::leds::led::led0);
    leds.toggle(sc::leds::led::led1);
    LOG_PRINTK("test");
  }
  return 0;
}
