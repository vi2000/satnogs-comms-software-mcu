/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "msg_arbiter.hpp"
#include <cstdint>
#include <satnogs-comms-proto/ota.pb.h>
#include <tinycrypt/sha256.h>

class ota
{
public:
  static constexpr size_t max_image_size = 192 * 1024;

  static ota &
  get_instance()
  {
    static ota instance;
    return instance;
  }

  /* Singleton */
  ota(ota const &) = delete;

  void
  operator=(ota const &) = delete;

  uint32_t
  context() const;

  bool
  active() const;

  uint32_t
  seq_num() const;

  void
  begin(const struct _ota_request &req, ota_tlm &resp);

  void
  finish(const struct _ota_finish &f, ota_tlm &resp);

  void
  packet(const struct _ota_data &d, ota_tlm &resp);

  void
  reset();

  uint32_t
  session_duration() const;

  void
  set_session_duration(uint32_t secs);

  void
  update();

private:
  uint32_t                      m_session_secs;
  uint32_t                      m_ctx;
  bool                          m_active;
  uint32_t                      m_seq_num;
  uint32_t                      m_image_size;
  msg_arbiter::msg              m_msg;
  uint8_t                       m_hash[32];
  struct tc_sha256_state_struct m_sha256_ctx;
  int64_t                       m_expire_ts;
  // struct flash_img_context      m_flash_img_ctx;

  ota();
};