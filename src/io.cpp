/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "io.hpp"
#include "callbacks.hpp"
#include "msg_arbiter.hpp"
#include <cstring>
#include <satnogs-comms/power.hpp>
#include <zephyr/canbus/isotp.h>
#include <zephyr/task_wdt/task_wdt.h>

namespace sc = satnogs::comms;

static struct k_thread can1_thread_data;
K_THREAD_STACK_DEFINE(can1_thread_stack, CONFIG_CAN1_THREAD_STACK_SIZE);

static struct k_thread radio_rx_thread_data;
K_THREAD_STACK_DEFINE(radio_rx_thread_stack, CONFIG_RADIO_RX_THREAD_STACK_SIZE);
static struct k_thread radio_tx_thread_data;
K_THREAD_STACK_DEFINE(radio_tx_thread_stack, CONFIG_RADIO_TX_THREAD_STACK_SIZE);

const struct device *can1_dev = DEVICE_DT_GET(DT_NODELABEL(fdcan1));

/* We want to accept messages from everyone */
static const struct isotp_msg_id can1_rx_filter = {
    .std_id   = CONFIG_CAN1_ISOTP_REMOTE_PEER_RX_ADDR,
    .ext_addr = 0,
    .dl       = 8,
    .flags    = 0};

static const struct isotp_msg_id can1_tx_conf = {
    .std_id = CONFIG_CAN1_ISOTP_TX_ADDR, .ext_addr = 0, .dl = 8, .flags = 0};
static const struct isotp_msg_id can1_tx_remote_conf = {
    .std_id   = CONFIG_CAN1_ISOTP_REMOTE_PEER_TX_ADDR,
    .ext_addr = 0,
    .dl       = 8,
    .flags    = 0};

static const struct isotp_msg_id can1_rx_conf = {
    .std_id = CONFIG_CAN1_ISOTP_RX_ADDR, .ext_addr = 0, .dl = 8, .flags = 0};
/*
 * Set the BS to 0 for receiving the ISOTP frame in a single isotp_recv_net()
 * call
 */
const struct isotp_fc_opts can1_fc_opts = {.bs = 8, .stmin = 0};

static struct isotp_recv_ctx can1_recv_ctx;
static struct isotp_send_ctx can1_send_ctx;

/*
 * Those messages are quite large and we do not want to take the memory from the
 * task stack
 */
static msg_arbiter::msg  can1_msg;
static sc::radio::rx_msg radio_rx_msg;
static msg_arbiter::msg  radio_msg_to_arb;
static msg_arbiter::msg  radio_msg_from_arb;

void
io::start()
{
  m_can1_tid =
      k_thread_create(&can1_thread_data, can1_thread_stack,
                      K_THREAD_STACK_SIZEOF(can1_thread_stack), can1_thread,
                      NULL, NULL, NULL, CONFIG_CAN1_THREAD_PRIO, 0, K_NO_WAIT);
  if (!m_can1_tid) {
    k_oops();
  }
  k_thread_name_set(m_can1_tid, "can1");

  m_radio_rx_tid = k_thread_create(&radio_rx_thread_data, radio_rx_thread_stack,
                                   K_THREAD_STACK_SIZEOF(radio_rx_thread_stack),
                                   radio_rx_thread, NULL, NULL, NULL,
                                   CONFIG_RADIO_RX_THREAD_PRIO, 0, K_NO_WAIT);

  m_radio_tx_tid = k_thread_create(&radio_tx_thread_data, radio_tx_thread_stack,
                                   K_THREAD_STACK_SIZEOF(radio_tx_thread_stack),
                                   radio_tx_thread, NULL, NULL, NULL,
                                   CONFIG_RADIO_RX_THREAD_PRIO, 0, K_NO_WAIT);

  if (!m_radio_rx_tid) {
    k_oops();
  }
  if (!m_radio_tx_tid) {
    k_oops();
  }
  k_thread_name_set(m_radio_rx_tid, "radio_rx");
  k_thread_name_set(m_radio_tx_tid, "radio_tx");
}

class can_enable_exception : public sc::exception
{
public:
  can_enable_exception(string_type file_name, numeric_type line, int err)
      : sc::exception(ETL_ERROR_TEXT("Could not enable CAN device", "canenbl"),
                      file_name, line, err)
  {
  }
};

class isotp_bind_exception : public sc::exception
{
public:
  isotp_bind_exception(string_type file_name, numeric_type line, int err)
      : sc::exception(
            ETL_ERROR_TEXT("Could not bind the ISOTP device", "isotpbind"),
            file_name, line, err)
  {
  }
};

class isotp_recv_exception : public sc::exception
{
public:
  isotp_recv_exception(string_type file_name, numeric_type line, int err)
      : sc::exception(
            ETL_ERROR_TEXT("Could not receive from ISOTP device", "isotprecv"),
            file_name, line, err)
  {
  }
};

void
io::can1_thread(void *arg1, void *arg2, void *arg3)
{
  int task_wdt_id = task_wdt_add(CONFIG_WATCHDOG_PERIOD_CAN1, task_wdt_callback,
                                 (void *)k_current_get());
  msg_arbiter    &arb = msg_arbiter::get_instance();
  auto           &pwr = sc::board::get_instance().power();
  struct net_buf *buf;

  pwr.enable(sc::power::subsys::CAN1);
  k_msleep(10);
  volatile int ret = can_start(can1_dev);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, can_enable_exception);

  ret = isotp_bind(&can1_recv_ctx, can1_dev, &can1_rx_filter, &can1_rx_conf,
                   &can1_fc_opts, K_FOREVER);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, isotp_bind_exception);

  while (1) {
    task_wdt_feed(task_wdt_id);

    ret = arb.pull(&can1_msg, msg_arbiter::subsys::CAN1, K_NO_WAIT);
    if (ret == 0) {
      ret = isotp_send(&can1_send_ctx, can1_dev, can1_msg.data, can1_msg.len,
                       &can1_tx_conf, &can1_tx_remote_conf, NULL, NULL);
    }

    can1_msg.iface = msg_arbiter::subsys::CAN1;
    can1_msg.len   = 0;
    do {
      ret = isotp_recv_net(&can1_recv_ctx, &buf, K_MSEC(100));
      if (ret == ISOTP_RECV_TIMEOUT) {
        /* This is ok! */
        break;
      }
      if (ret < 0) {
        SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, isotp_recv_exception);
      } else {
        std::memcpy(can1_msg.data + can1_msg.len, buf->data, buf->len);
        can1_msg.len += buf->len;
        net_buf_unref(buf);
        arb.push(&can1_msg);
      }
    } while (ret > 0);
  }
}

void
io::radio_rx_thread(void *arg1, void *arg2, void *arg3)
{
  int          task_wdt_id = task_wdt_add(CONFIG_WATCHDOG_PERIOD_RADIO_RX,
                                          task_wdt_callback, (void *)k_current_get());
  auto        &radio       = sc::board::get_instance().radio();
  msg_arbiter &arb         = msg_arbiter::get_instance();

  while (1) {
    task_wdt_feed(task_wdt_id);
    int ret = radio.recv_msg(radio_rx_msg, 1000);
    if (ret == 0) {
      radio_msg_to_arb.iface =
          radio_rx_msg.info.iface == sc::radio::interface::UHF
              ? msg_arbiter::subsys::RADIO_UHF
              : msg_arbiter::subsys::RADIO_SBAND;
      std::copy_n(radio_rx_msg.pdu, radio_rx_msg.info.len,
                  radio_msg_to_arb.data);
      arb.push(&radio_msg_to_arb);
    }
  }
}

void
io::radio_tx_thread(void *arg1, void *arg2, void *arg3)
{
  int          task_wdt_id = task_wdt_add(CONFIG_WATCHDOG_PERIOD_RADIO_TX,
                                          task_wdt_callback, (void *)k_current_get());
  auto        &radio       = sc::board::get_instance().radio();
  msg_arbiter &arb         = msg_arbiter::get_instance();

  while (1) {
    task_wdt_feed(task_wdt_id);
    int ret = arb.pull(&radio_msg_from_arb, msg_arbiter::subsys::RADIO_UHF,
                       K_MSEC(100));
    if (ret == 0) {
      radio.tx(sc::radio::interface::UHF, radio_msg_from_arb.data,
               radio_msg_from_arb.len);
    }
  }
}
