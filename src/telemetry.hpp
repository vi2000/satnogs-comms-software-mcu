/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <satnogs-comms-proto/satnogs-comms.pb.h>
#include <zephyr/kernel.h>

class telemetry
{
public:
  static constexpr uint32_t min_period_ms = 100;

  static telemetry &
  get_instance()
  {
    static telemetry tlm;
    return tlm;
  }

  static void
  power(satnogs_comms &msg);

  static void
  tlm(satnogs_comms &msg, telemetry_type type);

  static void
  ping(satnogs_comms &resp, const satnogs_comms &recv);

  void
  start();

  void
  enable(telemetry_type type, uint32_t period_ms);

  void
  stop();

  /* Singleton */
  telemetry(telemetry const &) = delete;

  void
  operator=(telemetry const &) = delete;

private:
  bool            m_enable_periodic;
  uint32_t        m_period_ms;
  telemetry_type  m_type;
  struct k_thread m_thread_data;
  k_tid_t         m_tid;

  telemetry();

  static void
  periodic_thread(void *arg1, void *arg2, void *arg3);

  static void
  to_pwr_msg(::power &msg);

  static void
  to_radio_uhf(radio_uhf &msg);

  static void
  to_radio_sband(radio_sband &msg);

  static void
  to_sensors_msg(sensors &msg);

  static void
  to_version(version &msg_hw, version &msg_lib, version &msg_fw);
};
