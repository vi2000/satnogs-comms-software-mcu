/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <cstddef>
#include <satnogs-comms/version.hpp>
namespace sc = satnogs::comms;

namespace satnogs
{
class version
{
public:
  static constexpr size_t fw_major = @PROJECT_VERSION_MAJOR@;
  static constexpr size_t fw_minor = @PROJECT_VERSION_MINOR@;
  static constexpr size_t fw_patch = @PROJECT_VERSION_PATCH@;

  /**
   * @brief Returns the numerical representation of the hardware version
   * that the library is compiled for
   *
   * @return constexpr size_t the numerical representation of the hardware version that
   * the library is compiled for
   */
  static constexpr size_t
  get_fw_version()
  {
    return comms::version::num(fw_major, fw_minor, fw_patch);
  }
};
} // namespace satnogs