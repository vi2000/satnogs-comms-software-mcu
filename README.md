# SatNOGS COMMS Software MCU
An open source CubeSat format communication sub-system.
![SatNOGS-COMMS](docs/assets/pcb-top.png)

## Development Guide
SatNOGS COMMS Software MCU in independent of any kind of development tool.
You can use the development environment of your choice.

### Requirements
* CMake (>= 3.20)
* C++17
* GNU Make
* Zephyr SDK (>= 0.16)
* Zephyr-RTOS (>=3.6.0)
* Device Tree Compiler (dtc)
* clang-format (>= 17.0)
* protobuf-compiler (protoc)

#### Optional
* openOCD (>= 0.11) (for testing and debugging)

### Dependencies
The SatNOGS COMMS Software MCU codebase depends on
[libsatnogs-comms](https://gitlab.com/librespacefoundation/satnogs-comms/libsatnogs-comms) and [satnogs-comms-proto](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-proto)
which are shipped as git submodules within the project.

### Build instructions

#### Prepare the Zephyr-RTOS development environment
1. Get the `west` tool
  ```bash
  pip3 install west
  ```
2. Fetch and install the `Zephyr-SDK`
  ```bash
  wget https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v0.16.1/zephyr-sdk-0.16.1_linux-x86_64.tar.xz
  tar xf zephyr-sdk-0.16.1_linux-x86_64.tar.xz
  cd zephyr-sdk-0.16.1
  ./setup.sh -t all -h -c
  ```
3. At a desired directory fetch the `Zephyr-RTOS` code-base using `west`
  ```bash
  west init --mr v3.6.0 zephyrproject
  ```
4. Export the necessary auto-generated `CMake` bindings and install necessary third-party tools
  ```bash
  cd zephyrproject
  west update
  west zephyr-export
  pip3 install --break-system-packages -r zephyr/scripts/requirements.txt
  ```
The above steps should be performed only once. You can now proceed to the next section to build the actual firmware.

##### Build the firmware
1. Clone the repository with all the necessary submodules
  ```bash
  git clone --recurse-submodules https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu.git
  ```
2. Configure and build the firmware for a specific hardware board revision.
This can be performed by utilizing the [Zephyr-RTOS specific board revision build process](https://docs.zephyrproject.org/latest/develop/application/index.html#building-for-a-board-revision).
For the available hardware versions refer to the [available hardware revisions](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware/-/tags).
For example, building the firmware for the 0.3 hardware revision would require the following commands:

  ```bash
  cd satnogs-comms-software-mcu
  mkdir build
  cd build
  cmake -DBOARD=satnogs_comms@0.3.0 ..
  make
  ```

> :warning: **If there is the need for a different board version build, deleting any previous cached files is highly recommended**

### Coding Style
For the C and C++ code, we use **LLVM** style.
Use `clang-format` and the options file `.clang-format` to
adapt to the styling.

At the root directory of the project there is the `clang-format` options
file `.clang-format` containing the proper configuration.
Developers can import this configuration to their favorite editor.
Failing to comply with the coding style described by the `.clang-format`
will result to failure of the automated tests running on our CI services.
So make sure that you import on your editor the coding style rules.

## Configuration
The majority of the configuration takes place with `Kconfig`.
To perform any customization, go to the build directory and execute `make menuconfig`


## Website and Contact
For more information about the project and Libre Space Foundation please visit our [site](https://libre.space/)
and our [community forums](https://community.libre.space).
You can also chat with the SatNOGS-COMMS development team at
https://riot.im/app/#/room/#satnogs-comms:matrix.org

## License
![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png)
&copy; 2019-2020 [Libre Space Foundation](https://libre.space).
