/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

 /* Current, voltage and temperature sensors, external pull-up */
 &i2c4 {
    pinctrl-0 = <&i2c4_scl_pb8 &i2c4_sda_pb7>;
    pinctrl-names = "default";
    status = "okay";
    clock-frequency = <I2C_BITRATE_STANDARD>;
    /* For bus recovery */
    scl-gpios = <&gpiob 8 GPIO_ACTIVE_HIGH>;
    sda-gpios = <&gpiob 7 GPIO_ACTIVE_HIGH>;
};

&adc1 {
    /*
     * adc1_inp16_pa0 -> imon_3v3_d
     */
    pinctrl-0 = <&adc1_inp16_pa0>;
    pinctrl-names = "default";
    st,adc-clock-source = <1>;
    st,adc-prescaler = <1>;
    status = "okay";
};

&adc3 {
    /*
     * adc3_inp0_pc2_c -> imon_5v_rf
     * adc3_inp1_pc3_c -> imon_5v_fpga
     */
    pinctrl-0 = <&adc3_inp0_pc2_c &adc3_inp1_pc3_c>;
    pinctrl-names = "default";
    st,adc-clock-source = <1>;
    st,adc-prescaler = <1>;
    status = "okay";
};

/ {
    aliases {
        sensorsi2c = &i2c4;
    };

    leds {
        compatible = "gpio-leds";
        en_uhf: en_uhf {
            gpios = <&gpioe 11 GPIO_ACTIVE_HIGH>;
            label = "ENABLE UHF";
        };

        /* Enable/Disable UHF AGC */
        en_agc_uhf: en_agc_uhf {
            gpios = <&gpiob 1 GPIO_ACTIVE_LOW>;
            label = "ENABLE AGC UHF";
        };

        /*
         * Filter select UHF
         * High -> Narrow
         * Low -> Wide
         */
        flt_sel_uhf: flt_sel_uhf {
            gpios = <&gpiob 0 GPIO_ACTIVE_HIGH>;
            label = "FLT SEL UHF";
        };

        en_sband: en_sband {
            gpios = <&gpiod 15 GPIO_ACTIVE_HIGH>;
            label = "ENABLE SBAND";
        };

        /* Enable/Disable UHF AGC */
        en_agc_sband: en_agc_sband {
            gpios = <&gpioa 3 GPIO_ACTIVE_LOW>;
            label = "ENABLE AGC SBAND";
        };

        /*
         * Filter select UHF
         * High -> Narrow
         * Low -> Wide
         */
        flt_sel_sband: flt_sel_sband {
            gpios = <&gpioa 2 GPIO_ACTIVE_HIGH>;
            label = "FLT SEL SBAND";
        };

    };

    gpio_keys {
        compatible = "gpio-keys";
        p5v_rf_pg: p5v_rf_pg {
            label = "P5V RF PGOOD";
            gpios = <&gpiod 9 GPIO_ACTIVE_HIGH>;
        };

        p5v_fpga_pg: p5v_fpga_pg {
            label = "P5V FPGA PGOOD";
            gpios = <&gpioe 12 GPIO_ACTIVE_HIGH>;
        };

        uhf_pgood: uhf_pgood {
            label = "UHF PGOOD";
            gpios = <&gpioe 10 GPIO_ACTIVE_HIGH>;
        };

        sband_pgood: sband_pgood {
            label = "SBAND PGOOD";
            gpios = <&gpioc 0 GPIO_ACTIVE_HIGH>;
        };

        alert_uhf_pa_temp: alert_uhf_pa_temp {
            label = "UHF PA TEMP ALERT";
            gpios = <&gpioe 8 GPIO_ACTIVE_LOW>;
        };

        alert_sband_pa_temp: alert_sband_pa_temp {
            label = "SBAND PA TEMP ALERT";
            gpios = <&gpiod 14 GPIO_ACTIVE_LOW>;
        };

        fpga_done: fpga_done {
            label = "FPGA DONE";
            gpios = <&gpiob 9 GPIO_ACTIVE_HIGH>;
        };
    };
};
